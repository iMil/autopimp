FROM debian:buster-slim

LABEL maintainer="imil@esunix.com"

ENV ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get --no-install-recommends -qq -y install \
    python python-pip gpg gpg-agent dirmngr \
    && echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" >> \
    /etc/apt/sources.list \
    && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367 \
    && apt-get update \
    && apt-get --no-install-recommends -qq -y install ansible \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /ansible

ENTRYPOINT ["ansible-playbook",  "-i", "hosts", "-l", "local"]
